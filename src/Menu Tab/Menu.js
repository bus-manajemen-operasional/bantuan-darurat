import React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';


import Divider from '@mui/material/Divider';

import { Container} from '@mui/system';

// import foto QR
import QR from '../assets/qr.png';
// import Stack from '@mui/material/Stack';

// import gambar warning
import warning from '../assets/bantuan darurat.png';
import { IconButton } from '@mui/material';



const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }
});



function Menu() {
    return (
        <ThemeProvider theme={theme}>
            <Box bgcolor='#303F51'>
                    <Container maxWidth="xl">
                        <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Bantuan Darurat</Typography>
                    </Container>
                    <Box>
                        <Grid container spacing={1} justifyContent='center'>
                            <Button>   
                            <Grid item>
                                <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                    <Typography variant='subtitle2'>Rute Operasional</Typography>
                                   
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Briefing</Typography>
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Go/No Go Item</Typography>
                            </Grid> 
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>  
                            <Grid item>
                                <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Riwayat Catatan</Typography>
                            </Grid>   
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                               
                            </Grid>  
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <WarningAmberRoundedIcon sx={{color: '#ECAB55', width: 60 , height: 60 , margin: 3.5}} />
                                <Typography variant='subtitle2' color='#ECAB55'>Bantuan Darurat</Typography>
                            </Grid>
                            </Button>
                        </Grid>
                    </Box>
                       
                    

                    {/* Gambar Bantuan Darurat */}
                    <Grid container justifyContent="center" alignItems="center" pt={8}>
                        <Grid Item>
                            <IconButton>
                                <Box component={'img'} src={warning}/>
                            </IconButton>
                        </Grid>
                    </Grid>

                    <Typography color='white' align='center' pt={3}>Dengan anda menekan tombol ini, maka anda telah mengirimkan sinyal</Typography>
                    <Typography color='white' align='center'>tanda meminta bantuan kepada armada terdekat dan tim tracking center</Typography>

                    <Divider sx={{borderColor: 'transparent', pt: 3}}/>

                    {/* <Container maxWidth='xl'>
                    <Box>
                        <Typography 
                        sx={{fontFamily: 'Montserrat', marginTop: 4, fontSize: 24}} 
                        color='white'
                        align='center'
                        >SERAH TERIMA OPERASIONAL</Typography> 
                    </Box>   
                    </Container>
                    */}
                   
                   
                    {/* bagian tombol QR code dan scan QRCODE */}
                    {/* <Container maxWidth="xl">
                    <Box display='flex' pt={5}>
                        
                        <Box width='50%'>
                            <Button variant="contained" color="success" sx={{width: '100%'}}>
                            <Typography fontFamily='Montserrat' fontSize={24} color='white'>QR CODE</Typography>
                            </Button>
                        </Box>
                        
                        <Box width='50%'>
                            <Button sx={{width: '100%', backgroundColor: '#D9D9D9', "&:hover": { backgroundColor: "grey"}}}>
                            <Typography fontFamily='Montserrat' fontSize={24} color='#AEAEAE'>SCAN QRCODE</Typography>
                            </Button>
                        </Box>
                    </Box>
                    </Container> */}


                    {/* Gambar Kode QR Nya */}
                    {/* <Container maxWidth="xl">
                    <Grid container direction="column" justifyContent='center' alignItems="center" spacing={0.5}>
                        <Grid item>
                            <Box 
                                component={'img'} 
                                src={QR}
                                pt={4}
                            />
                        </Grid>
                        
                        <Grid item>
                            <Typography fontFamily='Montserrat' color='white'>DZULKIFLI SUHERMAN</Typography>
                        </Grid>

                        <Grid item>
                            <Typography color='white'>0234819906</Typography>
                        </Grid>

                    </Grid>

                    <Divider sx={{borderColor: 'transparent', pt: 2}}/> 
                    </Container>  */}
          
        

            </Box>
        </ThemeProvider>
    );
}

export default Menu;

